package com.example.weatherapp.core.ui

import androidx.appcompat.app.AppCompatActivity
import com.example.weatherapp.core.util.hideKeyboard

/**
 * Created by JeeteshSurana.
 */

abstract class BaseActivity : AppCompatActivity() {

    override fun onDestroy() {
        hideKeyboard()
        super.onDestroy()
    }
}