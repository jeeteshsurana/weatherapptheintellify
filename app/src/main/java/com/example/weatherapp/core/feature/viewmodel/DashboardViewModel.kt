package com.example.weatherapp.core.feature.viewmodel

import android.content.Context
import com.example.weatherapp.R
import com.example.weatherapp.core.feature.repository.DashboardRepository
import com.example.weatherapp.core.model.response.WeatherResponse
import com.example.weatherapp.core.ui.BaseViewModel
import com.example.weatherapp.core.util.mutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Created by JeeteshSurana.
 */

class DashboardViewModel(
    private val context: Context,
    private val repository: DashboardRepository
) : BaseViewModel() {
    val weatherResponse = mutableLiveData(WeatherResponse())
    val weatherErrorResponse = mutableLiveData("")

    suspend fun getWeather(query: String) = withContext(Dispatchers.Main) {
        try {
            weatherResponse.postValue(
                repository.getWeather(
                    query,
                    context.resources.getString(R.string.weather_app_key),
                    context.resources.getString(R.string.units)
                )
            )
        } catch (e: Exception) {
            weatherErrorResponse.postValue(e.message)
        }
    }
}