package com.example.weatherapp.core.di

import android.content.Context
import com.example.weatherapp.connection.APIClient
import com.example.weatherapp.connection.HeaderInterceptor
import com.example.weatherapp.connection.NetworkInterceptor
import com.example.weatherapp.connection.RetrofitInterface
import com.example.weatherapp.core.feature.repository.DashboardRepository
import com.example.weatherapp.core.feature.viewmodel.DashboardViewModel
import com.example.weatherapp.core.ui.BaseViewModel
import com.example.weatherapp.core.util.NetworkManager
import com.example.weatherapp.core.util.PreferenceManager
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by JeeteshSurana.
 */

val appModule = module {

    // Repositories
    //network module
    single { NetworkManager(androidContext()) }
    single { APIClient(androidContext()) }
    //shared Preference
    single {
        PreferenceManager(
            androidContext().getSharedPreferences(
                androidContext().applicationContext.packageName,
                Context.MODE_PRIVATE
            )
        )
    }
    //network call setup
    single { NetworkInterceptor(androidContext()) }
    single { HeaderInterceptor() }
    single { RetrofitInterface(androidContext(),get(),get()) }
    single { DashboardRepository(get()) }

    //ViewModels
    viewModel { BaseViewModel() }
    viewModel { DashboardViewModel(androidContext(),get()) }
}