package com.example.weatherapp.core.feature.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.example.weatherapp.R
import com.example.weatherapp.core.feature.viewmodel.DashboardViewModel
import com.example.weatherapp.core.ui.BaseFragment
import com.example.weatherapp.core.util.hide
import com.example.weatherapp.core.util.hideKeyboard
import com.example.weatherapp.core.util.show
import com.example.weatherapp.core.util.snackbar
import com.example.weatherapp.databinding.DashboardBinding
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


/**
 * Created by JeeteshSurana.
 */
class DashboardFragment : BaseFragment() {

    private val mDashboardFragmentViewModel: DashboardViewModel by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val data: DashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        data.mData = mDashboardFragmentViewModel
        data.lifecycleOwner = this
        return data.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObserver()
    }

    private fun initObserver() {
        mDashboardFragmentViewModel.weatherResponse.observe(this, {
            it?.let { setUI() }
        })
        mDashboardFragmentViewModel.weatherErrorResponse.observe(this, {
            if (!it.isNullOrEmpty()) {
                cl_main.snackbar(it)
            }
        })
    }

    private fun setUI() {
        edit_query.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                validateData()
                return@OnEditorActionListener true
            }
            false
        })

        img_search.setOnClickListener {
            validateData()
        }
    }

    private fun validateData() {
        val query = edit_query.text.toString().trim()
        if (query.isNotEmpty()) getWeather(query)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
    }

    private fun init() {
        getWeather("Ahmedabad")
    }

    private fun getWeather(query: String) {
        lifecycleScope.launch {
            fl_progress.show()
            activity?.hideKeyboard()
            mDashboardFragmentViewModel.getWeather(query)
            fl_progress.hide()
        }
    }
}




