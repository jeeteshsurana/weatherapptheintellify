package com.example.weatherapp.core.model.error

data class WeatherErrorResponse(
    var cod: String? = null,
    var message: String? = null
)