package com.example.weatherapp.core.util

import android.content.ContextWrapper
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.example.weatherapp.R

/**
 * Created by JeeteshSurana.
 */

fun <T> mutableLiveData(defaultValue: T? = null): MutableLiveData<T> {
    val data = MutableLiveData<T>()
    if (defaultValue != null) {
        data.value = defaultValue
    }
    return data
}

fun View.getParentActivity(): AppCompatActivity? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }
        context = context.baseContext
    }
    return null
}


fun <T> ImageView.setPicture(obj: T, options: RequestOptions? = null, listener: RequestListener<Drawable>? = null) {
    Glide.with(this).load(obj).also {
        it.apply(options ?: RequestOptions.centerCropTransform().diskCacheStrategy(DiskCacheStrategy.ALL))
        if (listener != null) it.listener(listener)
        it.into(this)
    }
}

@BindingAdapter("setImageUrl")
fun setImageUrl(image: AppCompatImageView, url: String?) {
    if (!url.isNullOrBlank()) {
        image.setPicture(image.getParentActivity()?.resources?.getString(R.string.weather_image_url, url))
    }
}

@BindingAdapter("setTemp")
fun setTemp(view: AppCompatTextView, degree: String?) {
    if (!degree.isNullOrBlank()) {
        view.text = view.getParentActivity()?.resources?.getString(R.string.degree_value,degree)
    }
}

@BindingAdapter("setDate")
fun setDate(view: AppCompatTextView, dateValue: String?) {
    if (!dateValue.isNullOrBlank()) {
        view.text = getDateString(dateValue.toString().toLong())
    }
}
