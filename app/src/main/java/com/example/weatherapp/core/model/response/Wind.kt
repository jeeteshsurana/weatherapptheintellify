package com.example.weatherapp.core.model.response

data class Wind(
    var deg: Int? = null,
    var speed: Double? = null
)