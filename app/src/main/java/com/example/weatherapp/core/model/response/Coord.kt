package com.example.weatherapp.core.model.response

data class Coord(
    var lat: Double? = null,
    var lon: Double? = null
)