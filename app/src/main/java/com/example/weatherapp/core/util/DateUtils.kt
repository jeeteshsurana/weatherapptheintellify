package com.example.weatherapp.core.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * File holding all the methods of Date interest.
 * Created by JeeteshSurana.
 */

const val mCommonTimeFormat = "hh:mm"
private val simpleDateFormat = SimpleDateFormat(mCommonTimeFormat, Locale.ENGLISH)
fun getDateString(time: Long) : String = simpleDateFormat.format(time * 1000L)