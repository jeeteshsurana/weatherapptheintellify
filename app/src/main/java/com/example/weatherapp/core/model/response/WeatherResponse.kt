package com.example.weatherapp.core.model.response

data class WeatherResponse(
    var base: String? = null,
    var clouds: Clouds? = null,
    var cod: Int? = null,
    var coord: Coord? = null,
    var dt: Int? = null,
    var id: Int? = null,
    var main: Main? = null,
    var name: String? = null,
    var sys: Sys? = null,
    var timezone: Int? = null,
    var visibility: Int? = null,
    var weather: List<Weather>? = null,
    var wind: Wind? = null
)