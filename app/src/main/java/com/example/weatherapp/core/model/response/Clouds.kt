package com.example.weatherapp.core.model.response

data class Clouds(
    var all: Int? = null
)