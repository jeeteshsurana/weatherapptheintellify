package com.example.weatherapp.core.model.response

data class Weather(
    var description: String? = null,
    var icon: String? = null,
    var id: Int? = null,
    var main: String? = null
)