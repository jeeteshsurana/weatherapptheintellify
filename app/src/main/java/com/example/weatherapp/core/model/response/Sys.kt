package com.example.weatherapp.core.model.response

data class Sys(
    var country: String? = null,
    var id: Int? = null,
    var sunrise: Int? = null,
    var sunset: Int? = null,
    var type: Int? = null
)