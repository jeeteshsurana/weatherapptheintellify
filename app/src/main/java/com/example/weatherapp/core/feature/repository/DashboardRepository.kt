package com.example.weatherapp.core.feature.repository

import com.example.weatherapp.connection.RetrofitInterface
import com.example.weatherapp.connection.SafeApiRequest
import com.example.weatherapp.core.model.response.WeatherResponse

/**
 * Created by Abhin.
 */

class DashboardRepository(private val api: RetrofitInterface) : SafeApiRequest() {

    suspend fun getWeather(query: String, key: String,units: String): WeatherResponse {
        return apiRequest { api.getWeather(query, key,units) }
    }
}
