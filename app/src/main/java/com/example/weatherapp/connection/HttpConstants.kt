package com.example.weatherapp.connection

/**
 * Created by Jeetesh Surana.
 */

const val cacheControls: String = "Cache-Control"
const val baseURL: String = "https://api.openweathermap.org/"