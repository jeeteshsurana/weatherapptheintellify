package com.example.weatherapp

import android.os.Bundle
import com.example.weatherapp.core.feature.dashboard.DashboardFragment
import com.example.weatherapp.core.ui.BaseActivity
import com.example.weatherapp.core.util.addReplaceFragment

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addReplaceFragment(R.id.fl_container, DashboardFragment(), addFragment = false, false)
    }
}